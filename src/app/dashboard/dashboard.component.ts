import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(
    private router: Router,
  ) {

  }

  ngOnInit(): void {
  }

  selected_button(type: any, sidenav: any) {
    if (sidenav != null) {
      sidenav.close();
    }

    if (type == 'dashboard') {
      // this.showTable = true;
      // this.fillSlack = '#c20a24';
      // this.bgBc = {
      //   background: '#ffffffad'
      // }
      this.router.navigateByUrl('dashboard');
    } else if (type == 'siswa') {
      // this.showTable = true;
      // this.fillSlack = '#c20a24';
      // this.bgBc = {
      //   background: '#ffffffad'
      // }
      this.router.navigateByUrl('siswa');
    }
  };

}
