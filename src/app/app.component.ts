import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular';

  constructor(
    private router: Router,
  ) {

  }

  ngOnInit(): void {
  }

  selected_button(type: any, sidenav: any) {
    if (sidenav != null) {
      sidenav.close();
    }

    if (type == 'dashboard') {
      // this.showTable = true;
      // this.fillSlack = '#c20a24';
      // this.bgBc = {
      //   background: '#ffffffad'
      // }
      this.router.navigateByUrl('dashboard');
    } else if (type == 'siswa') {
      // this.showTable = true;
      // this.fillSlack = '#c20a24';
      // this.bgBc = {
      //   background: '#ffffffad'
      // }
      this.router.navigateByUrl('siswa');
    }
  };

}
